<?php declare(strict_types=1);

namespace FastOrder\Storefront\Controller;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItemFactoryRegistry;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class FastOrderController extends StorefrontController
{
    private LineItemFactoryRegistry $lineItemFactoryRegistry;
    private CartService $cartService;
    private EntityRepository $productRepository;
    private EntityRepository $fastOrdersAddedItemsRepository;

    public function __construct(LineItemFactoryRegistry $lineItemFactoryRegistry, CartService $cartService,
                                EntityRepository $productRepository, EntityRepository $fastOrdersAddedItemsRepository)
    {
        $this->lineItemFactoryRegistry = $lineItemFactoryRegistry;
        $this->cartService = $cartService;
        $this->productRepository = $productRepository;
        $this->fastOrdersAddedItemsRepository = $fastOrdersAddedItemsRepository;
    }

    /**
     * @Route("/fast-order", name="frontend.fast_order", methods={"GET"}, defaults={"_routeScope"={"storefront"}})
     */
    public function fastOrder(): Response
    {
        return $this->renderStorefront('@FastOrder/storefront/page/fast_order.html.twig', [
            'example' => 'Hello world'
        ]);
    }

    /**
     * @Route("/fast-order/process", name="frontend.fast_order_process", methods={"POST"}, defaults={"_routeScope"={"storefront"}})
     * @param Request $request
     * @param Cart $cart
     * @param SalesChannelContext $salesChannelContext
     * @param Context $context
     * @return RedirectResponse
     */
    public function processFastOrder(Request $request, Cart $cart, SalesChannelContext $salesChannelContext, Context $context): RedirectResponse
    {
        $addFastOrder = $request->request->get('add_fast_order');
        if(isset($addFastOrder)) {
            /**
             * Iterate through the 10 fields
             */
            for($i = 0; $i < 10; $i++) {
                /**
                 * Product quantity
                 */
                $quantity = (int)$request->request->get('quantity-article-'.$i);
                $articleNumber = $request->request->get('article-number-'.$i);

                $productData = $this->getProductData($context, $articleNumber, true, $quantity);
                if($productData !== null) {
                    /**
                     * Product Identifier
                     */
                    $productIdentifier = $productData[0];

                    /**
                     * Product exists
                     * Create product line item
                     */
                    $lineItem = $this->lineItemFactoryRegistry->create([
                        'type' => LineItem::PRODUCT_LINE_ITEM_TYPE,
                        'referencedId' => $productIdentifier,
                        'id' => $productIdentifier,
                        'quantity' => $quantity,
                        'payload' => ['key' => 'value']
                    ], $salesChannelContext);

                    /**
                     * Add the product line item in the cart
                     */
                    $this->cartService->add($cart, $lineItem, $salesChannelContext);

                    /**
                     * Generate UUID and save each article number and quantity to the database
                     */
                    $itemId = Uuid::randomHex();
                    $this->fastOrdersAddedItemsRepository->create([
                        [
                            'id' => $itemId,
                            'product_number' => $articleNumber,
                            'quantity' => $quantity,
                            'session_id' => session_id(),
                            'created_at' => date('Y-m-d H:i:s')
                        ]
                    ], $context);

                }
            }
        }

        return $this->redirectToRoute('frontend.checkout.cart.page');
    }

    /**
     * @Route("/fast-order/get-product-data", name="frontend.fast_order_product_data", methods={"POST"}, defaults={"csrf_protected"=false, "XmlHttpRequest"=true, "_routeScope"={"storefront"}})
     * @param Request $request
     * @param Context $context
     * @return JsonResponse
     */
    public function isProductAvailable(Request $request, Context $context): JsonResponse
    {
        $productNumber = $request->request->get('product_number');

        if(!isset($productNumber)) {
            return new JsonResponse([
                'is_available' => false,
                'error' => 'Product number is missing.'
            ]);
        }

        /**
         * Check if product is available and get data
         */
        $productData = $this->getProductData($context, $productNumber, false);

        /**
         * Product is not available
         */
        if($productData === null) {
            return new JsonResponse([
                'is_available' => false,
                'error' => 'Product isn\'t available.'
            ]);
        }

        /**
         * Product is available. Return price
         */
        return new JsonResponse([
            'is_available' => true,
            'price' => $productData[1]
        ]);
    }

    /**
     * Get product identifier and price to add to the cart if the product is available and for the form
     * @param Context $context
     * @param String $productNumber
     * @param bool $requestQuantity
     * @param int $quantity
     * @return array|null
     */
    protected function getProductData(Context $context, String $productNumber, bool $requestQuantity=true, int $quantity=0): ?array
    {
        /**
         * We check here for product number to get the reference ID and quantity, so we know how many products to add to the cart
         */
        if(($requestQuantity && (!empty(trim($productNumber)) && (!empty($quantity) && $quantity > 0))) || ($requestQuantity === false && !empty(trim($productNumber)))) {
            /**
             * Create criteria and add filter for productNumber
             */
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('productNumber', $productNumber));

            /**
             * Get product
             */
            $products = $this->productRepository->search($criteria, $context);

            /**
             * Check if product is available
             */
            $productElement = $products->getElements();
            if(!empty($productElement)) {
                $product = array_values($productElement)[0];

                if($product->availableStock > 0) {
                    /**
                     * Get price
                     */
                    $priceElement = $product->getPrice()->getElements();
                    if(!empty($priceElement)) {
                        $price = array_values($priceElement)[0];
                        /**
                         * Get gross price
                         */
                        $priceGross = $price->getGross();

                        return [array_values($productElement)[0]->getUniqueIdentifier(), $priceGross];
                    }

                    return [array_values($productElement)[0]->getUniqueIdentifier(), null];
                }

                return null;
            }

            return null;
        }

        return null;
    }
}
