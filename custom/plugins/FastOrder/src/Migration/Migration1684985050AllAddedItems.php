<?php declare(strict_types=1);

namespace FastOrder\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1684985050AllAddedItems extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1684985050;
    }

    public function update(Connection $connection): void
    {
        // implement update
        $query = <<<SQL
        CREATE TABLE IF NOT EXISTS `fast_orders_added_iteams` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `product_number` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
          `quantity` INT NOT NULL,
          `session_id` varchar(128) DEFAULT NULL,
          `created_at` datetime NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        SQL;

        $connection->executeStatement($query);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
