<?php declare(strict_types=1);

namespace FastOrder\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1685252228UpdatedAtField extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1685252228;
    }

    public function update(Connection $connection): void
    {
        // implement update

        $query = <<<SQL
        ALTER TABLE `fast_orders_added_items` ADD  `updated_at` datetime DEFAULT NULL AFTER `created_at`;
        SQL;

        $connection->executeStatement($query);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
