<?php declare(strict_types=1);

namespace FastOrder\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1684988336DropOldTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1684988336;
    }

    public function update(Connection $connection): void
    {
        // implement update
        $query = <<<SQL
DROP TABLE IF EXISTS `fast_orders_added_iteams`;
SQL;

        $connection->executeStatement($query);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
