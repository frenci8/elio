const { Criteria } = Shopware.Data;
import template from './fast-orders-list.html.twig';

Shopware.Component.register('fast-orders-list', {
    // Configuration here
    inject: ['repositoryFactory'],

    template,

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    data: function () {
        return {
            result: undefined,
            columns: [
                { property: 'created_at', label: 'Datetime Created' },
                { property: 'session_id', label: 'Session ID' },
                { property: 'product_number', label: 'Product Number' },
                { property: 'quantity', label: 'Quantity' },
            ],
        }
    },

    computed: {
        fastOrdersRepository() {
            // create a repository for the `fast_orders_added_items` entity
            return this.repositoryFactory.create('fast_orders_added_items');
        },
    },

    created() {
        const criteria = new Criteria();
        criteria.setPage(1);

        this.fastOrdersRepository.create('fast_orders_added_items');
        this.fastOrdersRepository
            .search(criteria, Shopware.Context.api)
            .then(result => {
                this.result = result;
                console.log(this.result);
            });
    }
});
