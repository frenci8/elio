import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Module.register('fast-order', {
    type: 'plugin',
    name: 'Fast Order',
    title: 'fast-order.general.mainMenuItemGeneral',
    description: 'fast-order.general.descriptionTextModule',
    color: '#ff3d58',
    icon: 'default-shopping-paper-bag-product',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'fast-orders-list',
            path: 'list'
        }
    },

    navigation: [{
        id: 'fast-ordermodule-list',
        label: 'fast-order.general.mainMenuItemGeneral',
        color: '#ff3d58',
        path: 'fast.order.list',
        icon: 'default-shopping-paper-bag-product',
        parent: 'sw-order',
        position: 100
    }],

    settingsItem: [{
        to: 'fast.order.list', // route to anything
        group: 'shop', // either system, shop or plugins
        icon: 'regular-shopping-cart',
    }]
});
