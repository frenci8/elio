import HttpClient from 'src/service/http-client.service';
import Plugin from 'src/plugin-system/plugin.class';

export default class FastOrderPlugin extends Plugin {
    init() {
        // initialize HttpClient
        this._client = new HttpClient();

        /**
         * Handle product number fields
         */
        let articles = document.getElementsByClassName('articles-numbers-fields');
        Array.from(articles).forEach((article) => {
            article.addEventListener('focusout', this.getArticleData.bind(this));
        });

        /**
         * Handle product quantities fields
         */
        let quantities = document.getElementsByClassName('quantity-articles-fields');
        Array.from(quantities).forEach((quantity) => {
            /**
             * When the user sets the value from the arrows in the input field
             */
           quantity.addEventListener('change', this.displayPricePerProduct.bind(this, quantity));
            /**
             * When the user sets the value from the keyboard
             */
            quantity.addEventListener('keyup', this.displayPricePerProduct.bind(this, quantity));
        });


        this.calculateTotalPrice();
    }

    getArticleData(data) {
        let articleField = data.target;
        let i = articleField.getAttribute('data-i');
        let articleNumber = articleField.value;
        if(articleNumber) {
            // send the ajax request
            this._client.post('/fast-order/get-product-data', JSON.stringify({product_number: articleNumber}), this.displayMessage.bind(this, articleField), 'application/json', false);
        } else {
            document.getElementById('row-'+i).classList.add('hide-i');
            document.getElementById('price-'+i).textContent="0";
            articleField.setAttribute('data-price', '');
            document.getElementById('price-row-'+i).classList.add('hide-i');
            this.calculateTotalPrice();
        }
    }

    displayMessage(articleField, data) {
        let i = articleField.getAttribute('data-i');

        let productData = JSON.parse(data);
        if(productData['is_available'] === false) {
            document.getElementById('price-row-'+i).classList.add('hide-i');
            document.getElementById('price-'+i).textContent="0";
            document.getElementById('article-number-'+i).setAttribute('data-price', '');
            document.getElementById('product-not-available-'+i).classList.remove('hide-i');
            document.getElementById('row-'+i).classList.remove('hide-i');
        } else {
            let productPrice = productData['price'];

            document.getElementById('row-'+i).classList.add('hide-i');
            document.getElementById('product-not-available-'+i).classList.add('hide-i');
            document.getElementById('article-number-'+i).setAttribute('data-price', productPrice);

            let quantityArticleI = document.getElementById('quantity-article-'+i).value;
            if(quantityArticleI && quantityArticleI > 0) {
                document.getElementById('price-'+i).textContent=String((quantityArticleI * productPrice).toFixed(2));
                document.getElementById('price-row-'+i).classList.remove('hide-i');
                document.getElementById('row-'+i).classList.remove('hide-i');
            }
        }

        this.calculateTotalPrice();
    }

    displayPricePerProduct(data) {
        let i = data.getAttribute('data-i');
        let quantity = data.value;
        let productPrice = document.getElementById('article-number-'+i).getAttribute('data-price');

        if(quantity && quantity > 0) {
            if(productPrice) {
                document.getElementById('price-'+i).textContent=String((quantity * productPrice).toFixed(2));
                document.getElementById('price-row-'+i).classList.remove('hide-i');
                document.getElementById('row-'+i).classList.remove('hide-i');
            } else {
                document.getElementById('price-row-'+i).classList.add('hide-i');
            }
        } else {
            document.getElementById('price-'+i).textContent="0";
            document.getElementById('price-row-'+i).classList.add('hide-i');
            if(!productPrice) {
                // document.getElementById('row-'+i).classList.add('hide-i');
            }
        }

        this.calculateTotalPrice();
    }

    calculateTotalPrice() {
        let totalAmount = 0;
        let totalAmountField = document.getElementById('total-amount');
        let prices = document.getElementsByClassName('all-prices');

        Array.from(prices).forEach((price) => {
            totalAmount += Number(price.innerText);
        });

        /**
         * Display the total amount in the fast order page
         */
        totalAmountField.innerText=totalAmount.toFixed(2);
    }

}
