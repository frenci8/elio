// Import all necessary Storefront plugins
import FastOrderPlugin from './FastOrder/fast-order.plugin';

// Register your plugin via the existing PluginManager
const PluginManager = window.PluginManager;
PluginManager.register('FastOrderPlugin', FastOrderPlugin);
